Summary: NixTux Conky for corporate machines
Name: nixtux-conky
Version: 0.1
Release: alt2
License: GPL
Group: System/Configuration/Packaging
Url: https://gitlab.com/nixtux-packaging/nixtux-conky.git
BuildArch: noarch

Source0: conky.conf
Source1: nixtux-conky.desktop

Packager: Mikhail Novosyolov <mikhailnov@dumalogiya.ru>

Requires: conky
Requires: lsb-release
Requires: coreutils

%description
This package contains NixTux Conky config for corporate machines

%prep
#%setup

%install
mkdir -p %buildroot%_sysconfdir/xdg/autostart/
mkdir -p %buildroot%_datadir/nixtux-conky/
install -p -m644 %{SOURCE0} %buildroot%_datadir/nixtux-conky/10-conky.conf
install -p -m644 %{SOURCE1} %buildroot%_sysconfdir/xdg/autostart/nixtux-conky.desktop

%files
%_sysconfdir/xdg/autostart/nixtux-conky.desktop
%_datadir/nixtux-conky/10-conky.conf

%changelog
* Tue Jul 13 2018 Mikhail Novosyolov <mikhailnov@dumalogiya.ru> 0.1-alt2
- initial build


#
PREFIX = /

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/etc/xdg/autostart
	install -m0644 ./nixtux-conky.desktop $(DESTDIR)/$(PREFIX)/etc/xdg/autostart/nixtux-conky.desktop
	
	install -d $(DESTDIR)/$(PREFIX)/usr/share/nixtux-conky
	install -m0644 ./conky.conf $(DESTDIR)/$(PREFIX)/usr/share/nixtux-conky/10-conky.conf
	
uninstall:
	rm -fv $(PREFIX)/usr/share/nixtux-conky/10-conky.conf
	rm -fv $(PREFIX)/etc/xdg/autostart/nixtux-conky.desktop
